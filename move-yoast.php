<?php
/*
Plugin Name: Move Yoast to Bottom !
Description: Ce plugin permet d'afficher la section de réglages du plugin "Wordpress SEO by Yoast" tout en bas de la page.
Version: 1.0
Author: Marc Perrien
Author URI: http://click.perrien.fr
*/

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

?>